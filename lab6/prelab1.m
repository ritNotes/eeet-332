init_bode{};

global s

H=4*(s+0.5)/s;

minreal(H)

make_bode('on','off',H,-2,1,0,60);
make_bode('off','on',H,-2,1,-90,0);
