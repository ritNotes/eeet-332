init_bode();

global s

p = (s)/(2*pi()*10000);

H=((1.1424)/((p^2)+(0.6265*p)+1.1424))*((0.6265)/(p+0.6265));

minreal(H)

make_bode('on','off',H,2,6,-40,5);
make_bode('off','on',H,2,6,-180,0);