function [cm,yy]=fft_ifft(t,y,N)
    m_ctr=-N/2:N/2+1;
    cm_ctr=fftshift(fft(y,N)/N);
    cm=fft(y,N)/N;
    make_stem(n,abs(cm_ctr),"Spectrum Amplitude","n","ab s(cm)");
    yy=real(ifft(N*cm_ctr));
    make_plot(t,yy,"Reconstructed Waveform","seconds","reconstructed Y");
end
