function [m_ct,cm_ctr_win,yy]=fft_hanning_ifft(t,y,N,Mwin)
    m_ctr=-N/2:N/2+1;
    cm_ctr=fftshift(fft(y,N)/N);
    cm=fft(y,N)/N;
    make_stem(n,abs(cm_ctr),"Spectrum Amplitude","n","ab s(cm)");
    win=zeros(size(N));
    win(m_ctr)=hanning(2*Mwin+1)';
    cm_ctr_win=cm_ctr*win;
    yy=real(ifft(N*cm_ctr_win));
    make_plot(t,yy,"Reconstructed Waveform","seconds","reconstructed Y");
end
