
init();
N=1024;
n=0:N-1;
T=9;
Ts=T/N;
t=0:Ts:T-Ts;
y=zeros(size(t));
t_between_1_and_2=find(t>=1&t<=2);
y(t_between_1_and_2)=sin(((2*pi)/(T*p))*t(t_between_1_and_2);
t_between_7_and_8=find(t>7&t<=8);
y(t_between_7_and_8)=sin(((2*pi)/(T*p))*t(t_between_7_and_8));
make_plot(t,y,'Input Function','n','y');
Mwin=128;
[m_ctr,cm_ctr,yy]=fft_hanning_ifft(t,n,y,N);
