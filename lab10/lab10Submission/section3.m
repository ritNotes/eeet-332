init();
N=16;
M=3;
m=-N/2:N/2-1;
cm=2*abs(m);
make_stem(m,cm,'Spectrum','m','cm');

win=zeros(size(m));
m_between_negM_and_posM=find(m>-M&m<M);
win(m_between_negM_and_posM)=hanning(2*M+1);
make_stem(m,win,'Window','m','win');
cm_win=cm.*win;
make_stem(m,cm_win,'Windowed Spectrum','m','cm_win');
