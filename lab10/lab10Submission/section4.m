init();
N=1024;
n=0:N-1;
T=9;
Ts=T/N;
A=10;
Tsine=1/2;
t=0:Ts:T-Ts;
y=zeros(size(t));
t_between_1_and_2=find(t>=1&t<=2);
y(t_between_1_and_2)=A*sin(((2*pi)/(Tsine))t(t_between_1_and_2));
t_between_3_and_5=find(t>3&t<=5);
y(t_between_7_and_8)=2*t(t_between_7_and_8)+6;
make_plot(t,y,'Input Function','n','y');
Mwin=128;
[m_ctr,cm_ctr,yy]=fft_hanning_ifft(t,n,y,N);
