init();
N=1024;
n=0:N-1;
T=9;
Ts=T/N;
A=10;
Tsine=1/2;
t=0:Ts:T-Ts;
w1=8*2*pi;
w2=15*2*pi;
y=10+5*sin(w1*t)+16*sin(w2*t);
make_plot(t,y,'Input Function','n','y');
Mwin=128;
[m_ctr,cm_ctr,yy]=fft_hanning_ifft(t,n,y,N);
