init();
N=64;
n=0:N-1;
nc=-N/2:N/2-1;
T=5;
Ts=T/N;
t=n*Ts;
y=zeros(size(t));
wsig=2*pi/4;
ind1=find(t<1);
ind2=find(t>4);
y(ind1)=2*cos(wsig*t(ind1));
y(ind2)=2*sin(wsig*t(ind2));
make_plot(t,y,'Split Sine wave (N=64)',"seconds (n*Ts)","y");
[cm,yy]=fft_ifft(t,n,y,N);
