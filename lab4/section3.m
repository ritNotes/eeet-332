init();
disp('Section 3: Finding Transfer Function');
syms s R L C Zc Zp TF
eqn1 = Zp==R+L*s;
Zp = solve(eqn1,Zp)
eqn2 = Zc==1/(s*C);
Zc = solve(eqn2,Zc)
eqn3 = TF==Zc/(Zc+Zp);
TF = solve(eqn3,TF)