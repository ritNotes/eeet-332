disp('Section 1: Finding Transfer Function');
syms s R L C Zc Zp TF
eqn1 = Zp==R*s*L/(R+s*L);
Zp = solve(eqn1,Zp)
eqn2 = Zc==1/(s*C);
Zc = solve(eqn2,Zc)
eqn3 = TF==Zp/(Zc+Zp);
TF = solve(eqn3,TF)
R=2; L=5; C=2;
TF = subs(TF)