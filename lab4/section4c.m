init();
disp('Section 1: Finding Transfer Function');
syms s R L C Zc Zp TF
eqn1 = Zp==R+L*s;
Zp = solve(eqn1,Zp)
eqn2 = Zc==1/(s*C);
Zc = solve(eqn2,Zc)
eqn3 = TF==Zc/(Zc+Zp);
TF = solve(eqn3,TF)
R=4; L=1; C=1;
TF = subs(TF)
[symNum,symDen]=numden(TF)
num=sym2poly(symNum)
den=sym2poly(symDen)
polyTF=tf(num,den)
[yi,t]=impulse(polyTF);
roots(den)
make_plot(t,yi,'Section 4 Impulse Response','t','yi');