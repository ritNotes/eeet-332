init();
M=11;
T=1E-3;
[m,cm]=create_cm_series(M,T);
t=0:0.002/500:0.002;
y=cm2yt(t,T,m,cm,M);
make_stem(m,cm,"Square Wave Stem Plot","Harmonics"," ");
make_plot(t,real(y),"Square Wave with 11 Harmonics","t (sec)","yin");